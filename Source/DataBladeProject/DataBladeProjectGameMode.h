// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DataBladeProjectGameMode.generated.h"

UCLASS(minimalapi)
class ADataBladeProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADataBladeProjectGameMode();
};



