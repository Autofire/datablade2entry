// Copyright Epic Games, Inc. All Rights Reserved.

#include "DataBladeProjectGameMode.h"
#include "DataBladeProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADataBladeProjectGameMode::ADataBladeProjectGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
