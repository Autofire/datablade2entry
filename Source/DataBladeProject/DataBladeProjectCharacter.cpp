// Copyright Epic Games, Inc. All Rights Reserved.

#include "DataBladeProjectCharacter.h"

#include "Ball.h"

#include "Math/UnrealMathUtility.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"



//////////////////////////////////////////////////////////////////////////
// ADataBladeProjectCharacter

ADataBladeProjectCharacter::ADataBladeProjectCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	// Create the hitbox used for "striking" the ball
	HitBoxCollider = CreateDefaultSubobject<UCapsuleComponent>(TEXT("HitBox"));
	HitBoxCollider->SetupAttachment(RootComponent);
	//HitBoxCollider->InitCapsuleSize(72.f, 66.f);
	HitBoxCollider->OnComponentBeginOverlap.AddDynamic(this, &ADataBladeProjectCharacter::OnHitBoxOverlapBegin);
	HitBoxCollider->OnComponentEndOverlap.AddDynamic(this, &ADataBladeProjectCharacter::OnHitBoxOverlapEnd);

	// Set hitbox parameters
	HitBaseImpulse = FVector(0.f, 0.f, 50000.f);
	HitImpulseDeviationMin  =  5000.f;
	HitImpulseDeviationMax  = 12000.f;
	HitDirectionalMagnitude = 10000.f;
	DebugHitBoxOverlaps = false; 
	DebugHitBoxImpulse = false;

	// Set internal variables
	overlappingBall = nullptr;
}

//////////////////////////////////////////////////////////////////////////
// Input

void ADataBladeProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ADataBladeProjectCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ADataBladeProjectCharacter::MoveRight);

	PlayerInputComponent->BindAction("Hit", IE_Pressed, this, &ADataBladeProjectCharacter::TryHitBall);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ADataBladeProjectCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ADataBladeProjectCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ADataBladeProjectCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ADataBladeProjectCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ADataBladeProjectCharacter::OnResetVR);
}


void ADataBladeProjectCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ADataBladeProjectCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void ADataBladeProjectCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void ADataBladeProjectCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ADataBladeProjectCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ADataBladeProjectCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ADataBladeProjectCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ADataBladeProjectCharacter::TryHitBall() {
	if (overlappingBall) {
		HitBall(overlappingBall);
	}
}

void ADataBladeProjectCharacter::HitBall(ABall* ball) {
	UPrimitiveComponent* BallRB = ball->GetPhysicsComponent();

	if (BallRB != nullptr) {

		// To make things nice an consistent, we'll want to wipe all of the existing
		// momentum on the ball. Once we do that, we can start applying our own
		// impulses.
		BallRB->SetAllPhysicsLinearVelocity(FVector::ZeroVector);

		// First, we always apply the base impulse.
		// We always want to send the ball up, after all.
		BallRB->AddImpulse(HitBaseImpulse);

		// Then we want to apply the random deviation.
		// This will make it a bit more unpredictable.
		FVector DeviationDirection = FVector(FMath::RandRange(-1.f, 1.f), FMath::RandRange(-1.f, 1.f), 0.f);
		if (DeviationDirection.Normalize()) {
			float DeviationMagnitude = FMath::RandRange(HitImpulseDeviationMin, HitImpulseDeviationMax);
			BallRB->AddImpulse(DeviationDirection * DeviationMagnitude);

			if (GEngine && DebugHitBoxImpulse)
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("Random impulse: %s"), *(DeviationDirection * DeviationMagnitude).ToString()));
		}

		// Finally, we'll also apply whatever directional impulse due to our
		// position relative to the ball.
		//FVector OurPos = GetActorLocation();
		//FVector BallPos = OtherBall->GetActorLocation();
		FVector DirectionalImpulseDir = ball->GetActorLocation() - GetActorLocation();
		DirectionalImpulseDir.Z = 0.f; // We're only considering the XY-plane distance here.
		float DistFromBall = DirectionalImpulseDir.Size();
		float MaxDirectionalDist = HitBoxCollider->GetScaledCapsuleRadius();
		float DirectionalImpulseMag = HitDirectionalMagnitude * FMath::Clamp(DistFromBall / MaxDirectionalDist, 0.f, 1.f);

		if (DirectionalImpulseDir.Normalize()) {
			BallRB->AddImpulse(DirectionalImpulseDir * DirectionalImpulseMag);

			if (GEngine && DebugHitBoxImpulse)
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("Directional impulse: %s"), *(DirectionalImpulseDir * DirectionalImpulseMag).ToString()));
		}

		// Finally, we need to tell the ball it just got hit
		ball->IncreaseHitCount();

	}
	else {

		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Cast failed!")));
	}
}

void ADataBladeProjectCharacter::OnHitBoxOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
	{
		ABall* OtherBall = dynamic_cast<ABall*>(OtherActor);

		if (DebugHitBoxOverlaps)
		{
			if (OtherBall) {
				if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("I'm now overlapping the ball!")));
				if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("I'm now overlapping: %s"), *OtherBall->GetRootComponent()->GetName()));
			}
			else {
				if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("I'm now overlapping: %s"), *OtherActor->GetName()));
			}
		}

		if (OtherBall) {
			overlappingBall = OtherBall;
		}
	}
}


void ADataBladeProjectCharacter::OnHitBoxOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
	{
		ABall* OtherBall = dynamic_cast<ABall*>(OtherActor);

		if (DebugHitBoxOverlaps)
		{
			if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("I'm no longer overlapping: %s"), *OtherActor->GetName()));
		}

		if (OtherBall == overlappingBall) {
			overlappingBall = nullptr;
		}
	}
}
