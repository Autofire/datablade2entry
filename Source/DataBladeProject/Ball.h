// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Ball.generated.h"

UCLASS()
class DATABLADEPROJECT_API ABall : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABall();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USphereComponent* SphereRoot;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* SphereRenderer;

	UPROPERTY(EditAnywhere, Category = Debugging)
		bool DebugCount;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	int hitCount;


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
		UPrimitiveComponent* GetPhysicsComponent();

	UFUNCTION(BlueprintNativeEvent, Category = "Custom")
	void IncreaseHitCount();
	void IncreaseHitCount_Implementation();

	UFUNCTION(BlueprintNativeEvent, Category = "Custom")
	void ResetHitCount();
	void ResetHitCount_Implementation();

	UFUNCTION(BlueprintCallable, Category = "Custom")
	void SetHitCount(int newValue)
		{ hitCount = newValue; }

	UFUNCTION(BlueprintPure, Category = "Custom")
	int GetHitCount() const
		{ return hitCount; }


};
