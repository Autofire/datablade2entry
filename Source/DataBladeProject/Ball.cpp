// Fill out your copyright notice in the Description page of Project Settings.

#include "Ball.h"

#include "DataBladeProjectCharacter.h"

#include "Engine/StaticMesh.h"
#include "Engine/EngineTypes.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"


#define INIT_RADIUS 40.0f
#define SHAPE_INIT_RADIUS 50.0f

// Sets default values
ABall::ABall()
{

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use a sphere as a simple collision representation
	SphereRoot = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	SphereRoot->SetSimulatePhysics(true);
	SphereRoot->SetNotifyRigidBodyCollision(true);
	SphereRoot->InitSphereRadius(INIT_RADIUS);

	SphereRoot->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
	SphereRoot->OnComponentHit.AddDynamic(this, &ABall::OnCompHit);

	// Set as root component
	RootComponent = SphereRoot;


	SphereRenderer = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Renderer"));
	SphereRenderer->SetupAttachment(RootComponent);
	SphereRenderer->SetRelativeLocation(FVector(0.0f, 0.0f, -INIT_RADIUS));
	SphereRenderer->SetWorldScale3D(FVector(INIT_RADIUS / SHAPE_INIT_RADIUS));

	SphereRenderer->SetSimulatePhysics(false);
	SphereRenderer->BodyInstance.SetCollisionProfileName("NoCollision");

	DebugCount = false;
}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();

	hitCount = 0;
}

// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABall::OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
	{
		//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("I Hit: %s"), *OtherActor->GetName()));

		// We don't want to reset the hit count if we hit the player
		if (dynamic_cast<ADataBladeProjectCharacter*>(OtherActor) == nullptr) {
			ResetHitCount();
		}
	}
}

UPrimitiveComponent* ABall::GetPhysicsComponent()
{
	return dynamic_cast<UPrimitiveComponent*>(GetRootComponent());
}

void ABall::IncreaseHitCount_Implementation() {
	hitCount++;

	if (GEngine && DebugCount) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Hit #%d"), hitCount));
}

void ABall::ResetHitCount_Implementation() {
	if (hitCount > 0) {
		hitCount = 0;

		if (GEngine && DebugCount) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Combo BROKEN")));
	}
}
